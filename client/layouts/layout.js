import { Template } from 'meteor/templating';
import './layout.html';

Template.layout.helpers({
  menuItems() {
    return [
      {title: 'What we do', href: '#whatwedo'},
      {title: 'Clients', href: '#clients'},
      {title: 'Meet the team', href: '/team'},
      {title: 'Career', href: '/career'},
      {title: 'Let`s Talk', href: '/letstalk', talkBtn: true}
    ];
  }
});

Template.layout.events({
  'click #showhide'(event) {
    let mobMenu = document.querySelector(".mobile-menu");
    let body = document.querySelector("body");
    
    if(mobMenu) {
      mobMenu.classList.add("maxheight");
    }
    
    if(body) {
      body.classList.add("nonscrollable");
    }
  },
  
  'click #hideshow'() {
    let mobMenu = document.querySelector(".mobile-menu");
    let body = document.querySelector("body");
  
    if(mobMenu) {
      mobMenu.classList.remove("maxheight");
    }
  
    if(body) {
      body.classList.remove("nonscrollable");
    }
  },
  
  'click #talkbutton'() {
    let body = document.querySelector("body");
    let popup = document.querySelector(".popup-wrapper");
    
    body.classList.add("nonscrollable");
    popup.classList.add("dis-block");
    popup.classList.add("maxheight");
  },
  
  'click #talkbutton1'() {
    let body = document.querySelector("body");
    let mobMenu = document.querySelector(".mobile-menu");
    let popup = document.querySelector(".popup-wrapper");
    
    body.classList.add("nonscrollable");
    mobMenu.classList.remove("maxheight");
    popup.classList.add("dis-block");
    popup.classList.add("maxheight");
  },
  
  'click .btn-close'() {
    let body = document.querySelector("body");
    let popup = document.querySelector(".popup-wrapper");
    
    popup.classList.remove("maxheight");
    popup.classList.remove("dis-block");
    body.classList.remove("nonscrollable");
  }
});
